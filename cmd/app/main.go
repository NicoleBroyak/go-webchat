package main

import (
	"log"
	"net/http"

	"go_webchat/actions"

	"github.com/gobuffalo/buffalo/servers"
	"github.com/gobuffalo/envy"
)

// main is the starting point for your Buffalo application.
// You can feel free and add to this `main` method, change
// what it does, etc...
// All we ask is that, at some point, you make sure to
// call `app.Serve()`, unless you don't want to start your
// application that is. :)
func main() {
	app := actions.App()
	var ENV = envy.Get("GO_ENV", "test")

	if ENV != "production" {
		httpServer := &http.Server{}
		server := servers.WrapTLS(httpServer, "server.crt", "server.key")

		if err := app.Serve(server); err != nil {
			log.Fatal(err)
		}
	} else {
		if err := app.Serve(); err != nil {
			log.Fatal(err)
		}

	}
}

/*
# Notes about `main.go`

## SSL Support

We recommend placing your application behind a proxy, such as
Apache or Nginx and letting them do the SSL heavy lifting
for you. https://gobuffalo.io/en/docs/proxy

## Buffalo Build

When `buffalo build` is run to compile your binary, this `main`
function will be at the heart of that binary. It is expected
that your `main` function will start your application using
the `app.Serve()` method.

*/
