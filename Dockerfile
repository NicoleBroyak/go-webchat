# This is a multi-stage Dockerfile and requires >= Docker 17.05
# https://docs.docker.com/engine/userguide/eng-image/multistage-build/
FROM gobuffalo/buffalo:v0.18.12 as builder

ENV GOPROXY http://proxy.golang.org

RUN mkdir -p /src/go_webchat
WORKDIR /src/go_webchat

ADD package.json .
ADD yarn.lock .yarnrc.yml ./
RUN mkdir .yarn
COPY .yarn .yarn
RUN npm install -g npm@9.4.0
RUN wget https://nodejs.org/dist/v18.13.0/node-v18.13.0-linux-x64.tar.xz
RUN tar xvf node-v18.13.0-linux-x64.tar.xz 
WORKDIR /src/go_webchat/node-v18.13.0-linux-x64
RUN cp -rf bin/ /usr && cp -rf include/ /usr && cp -rf lib/ /usr && cp -rf share/ /usr
WORKDIR /src/go_webchat
RUN npm install -g yarn
RUN yarn install
# Copy the Go Modules manifests
COPY go.mod go.mod
COPY go.sum go.sum
# cache deps before building and copying source so that we don't need to re-download as much
# and so that source changes don't invalidate our downloaded layer
RUN go mod download

ADD . .
RUN buffalo build --static -o /bin/app

FROM alpine
RUN apk add --no-cache bash
RUN apk add --no-cache ca-certificates

WORKDIR /bin/

COPY --from=builder /bin/app .

# Uncomment to run the binary in "production" mode:
ENV GO_ENV=production

# Bind the app to 0.0.0.0 so it can be seen from outside the container
ENV ADDR=0.0.0.0

EXPOSE 3000

# Uncomment to run the migrations before running the binary:

CMD /bin/app migrate; /bin/app
# CMD exec tail -f /dev/null
