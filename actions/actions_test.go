package actions

import (
	"testing"

	"github.com/gobuffalo/suite/v4"
)

type ActionSuite struct {
	*suite.Action
	Token string
}

func Test_ActionSuite(t *testing.T) {
	action := suite.NewAction(App())

	as := &ActionSuite{
		Action: action,
	}

	suite.Run(t, as)
}
