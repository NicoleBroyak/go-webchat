package actions

import "github.com/gobuffalo/buffalo"

func UserChatJunctionCreate(c buffalo.Context) error {
	return c.Render(200, r.JSON(""))
}

func UserChatJunctionUpdate(c buffalo.Context) error {
	return c.Render(200, r.JSON(""))
}

func UserChatJunctionsForUser(c buffalo.Context) error {
	return c.Render(200, r.JSON(""))
}

func UserChatJunctionsForChat(c buffalo.Context) error {
	return c.Render(200, r.JSON(""))
}
