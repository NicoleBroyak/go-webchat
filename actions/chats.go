package actions

import (
	"fmt"
	"net/http"

	"github.com/gobuffalo/buffalo"
	"github.com/gobuffalo/pop/v6"
	"github.com/gofrs/uuid"
	"github.com/pkg/errors"

	"go_webchat/logger"
	"go_webchat/models"
)

type CurrentUserChatsModel struct {
	models.Chat
	models.Users
}

func CurrentUserChats(c buffalo.Context) error {
	userID, ok := c.Session().Get("current_user_id").(uuid.UUID)
	if !ok {
		return c.Redirect(http.StatusUnauthorized, "/auth")
	}
	tx := c.Value("tx").(*pop.Connection)

	userChatJunctions := models.UserChatJunctions{}
	err := tx.Where("user_id = ?", userID.String()).All(&userChatJunctions)
	if err != nil {
		logger.L.Error(err)
		return errors.WithStack(err)
	}

	userChatsIDs := []string{}

	for _, userChatJunction := range userChatJunctions {
		userChatsIDs = append(userChatsIDs, userChatJunction.ChatID.String())
	}

	logger.L.Info(userChatsIDs)
	logger.L.Info(userID.String())

	chats := models.Chats{}

	err = tx.Where("id in (?)", userChatsIDs).All(&chats)
	logger.L.Info(chats)

	jsonModel := []CurrentUserChatsModel{}
	for _, chat := range chats {
		chatUsers := models.UserChatJunctions{}
		err := tx.Where("chat_id = ?", chat.ID.String()).All(&chatUsers)
		if err != nil {
			logger.L.Error(err)
			return errors.WithStack(err)
		}

		userIDs := []string{}

		for _, userChatJunction := range chatUsers {
			userIDs = append(userIDs, userChatJunction.UserID.String())
		}

		users := models.Users{}

		err = tx.Where("id in (?)", userIDs).All(&users)
		logger.L.Info(chats)

		model := CurrentUserChatsModel{Chat: chat, Users: users}
		jsonModel = append(jsonModel, model)
	}

	logger.L.Info(jsonModel)

	return c.Render(http.StatusOK, r.JSON(jsonModel))
}

func ChatsList(c buffalo.Context) error {
	// Get the DB connection from the context
	tx, ok := c.Value("tx").(*pop.Connection)
	if !ok {
		return fmt.Errorf("no transaction found")
	}

	chats := &models.Chats{}

	if err := tx.All(chats); err != nil {
		return err
	}

	return c.Render(200, r.JSON(chats))
}

func ChatsCreateFromForm(c buffalo.Context) error {
	chat := &models.Chat{}
	if err := c.Bind(chat); err != nil {
		return errors.WithStack(err)
	}

	tx := c.Value("tx").(*pop.Connection)
	err := tx.Create(chat)
	if err != nil {
		return errors.WithStack(err)
	}

	c.Flash().Add("success", "Successfully created chat")

	return c.Redirect(302, "/")
}

func ChatsCreateWithUser(c buffalo.Context) error {
	logger.L.Info("START")
	user := &models.User{}
	secondUser := &models.User{}

	currentUserID := c.Session().Get("current_user_id").(uuid.UUID)
	secondUserIDStr := c.Params().Get("user_id")
	secondUserID, _ := uuid.FromString(secondUserIDStr)

	tx, ok := c.Value("tx").(*pop.Connection)
	if !ok {
		return fmt.Errorf("no transaction found")
	}

	err := tx.Find(user, currentUserID.String())
	logger.L.Info(err)
	if err != nil {
		return errors.WithStack(err)
	}

	err = tx.Find(secondUser, secondUserID)
	logger.L.Info(err)
	if err != nil {
		return errors.WithStack(err)
	}

	chatName := fmt.Sprintf("%v & %v", user.Nickname, secondUser.Nickname)
	altChatName := fmt.Sprintf("%v & %v", secondUser.Nickname, user.Nickname)

	chat := &models.Chat{Name: chatName}
	exists, err := tx.Where("name = ? OR name = ?", chatName, altChatName).Exists(&models.Chat{})
	if exists {
		c.Flash().Add("danger", "chat already exists")
		return c.Render(http.StatusOK, r.HTML("index.html"))
	}
	err = tx.Create(chat)
	logger.L.Info(err)
	if err != nil {
		return errors.WithStack(err)
	}

	err = tx.Select("id").Where("name = ? ", chatName).First(chat)
	if err != nil {
		return errors.WithStack(err)
	}

	userChatJunctions := models.UserChatJunctions{
		models.UserChatJunction{
			UserID: secondUserID,
			ChatID: chat.ID,
		},
		models.UserChatJunction{
			UserID: currentUserID,
			ChatID: chat.ID,
		},
	}

	for _, junction := range userChatJunctions {
		err = tx.Create(&junction)
		if err != nil {
			return errors.WithStack(err)
		}
	}

	c.Flash().Add("success", fmt.Sprintf("Successfully created chat with %v", secondUser.Nickname))

	logger.L.Info("END")
	return c.Render(http.StatusOK, r.HTML("index.html"))
}
