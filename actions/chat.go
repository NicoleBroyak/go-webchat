package actions

import (
	"fmt"
	"go_webchat/models"
	"net/http"

	"github.com/gobuffalo/buffalo"
	"github.com/gobuffalo/pop/v6"
)

// ChatHome default implementation.
func ChatHome(c buffalo.Context) error {
	return c.Render(http.StatusOK, r.HTML("index.html"))
}

func ChatView(c buffalo.Context) error {
	chatID := c.Params().Get("chat_id")
	tx, ok := c.Value("tx").(*pop.Connection)
	if !ok {
		return fmt.Errorf("no transaction found")
	}
	chat := &models.Chat{}
	err := tx.Find(chat, chatID)
	if err != nil {
		c.Flash().Add("danger", "chat id not found")
		return c.Render(http.StatusOK, r.HTML("index.html"))
	}

	return c.Render(http.StatusOK, r.HTML("index.html"))
}

func ChatDetails(c buffalo.Context) error {
	chatID := c.Params().Get("chat_id")
	tx, ok := c.Value("tx").(*pop.Connection)
	if !ok {
		return fmt.Errorf("no transaction found")
	}
	chat := &models.Chat{}
	err := tx.Find(chat, chatID)
	if err != nil {
		c.Flash().Add("danger", "chat id not found")
		return c.Render(http.StatusOK, r.HTML("index.html"))
	}

	return c.Render(http.StatusOK, r.JSON(chat))
}

func ChatUsers(c buffalo.Context) error {
	chatID := c.Params().Get("chat_id")
	// Get the DB connection from the context
	tx, ok := c.Value("tx").(*pop.Connection)
	if !ok {
		return fmt.Errorf("no transaction found")
	}

	chat := &models.Chat{}

	if err := tx.Find(chat, chatID); err != nil {
		return c.Render(200, r.JSON(nil))
	}

	usersChatJunction := models.UserChatJunctions{}

	err := tx.Select("user_id").Where("chat_id = ?", chatID).All(&usersChatJunction)
	if err != nil {
		return c.Render(200, r.JSON(nil))
	}

	usersChatsIDs := []string{}

	for _, usersChatJunction := range usersChatJunction {
		usersChatsIDs = append(usersChatsIDs, usersChatJunction.UserID.String())
	}

	users := &models.Users{}
	err = tx.Select("id, nickname").Where("id in (?)", usersChatsIDs).All(users)

	return c.Render(200, r.JSON(users))
}
