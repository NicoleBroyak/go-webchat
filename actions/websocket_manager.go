package actions

import (
	"context"
	"encoding/json"
	"fmt"
	"go_webchat/logger"
	"go_webchat/models"
	"log"
	"net/http"
	"sync"
	"time"

	"github.com/pkg/errors"

	"github.com/gobuffalo/buffalo"
	"github.com/gofrs/uuid"
	"github.com/gorilla/websocket"
)

var (
	websocketUpgrader = websocket.Upgrader{
		CheckOrigin:     checkOrigin,
		ReadBufferSize:  1024,
		WriteBufferSize: 1024,
	}
)

type Manager struct {
	clients ClientList
	sync.RWMutex

	handlers map[string]EventHandler
}

func NewManager(ctx context.Context) *Manager {
	m := &Manager{
		clients:  make(ClientList),
		handlers: make(map[string]EventHandler),
	}

	m.setupEventHandlers()

	return m
}

func (m *Manager) setupEventHandlers() {
	m.handlers[EventSendMessage] = SendMessage
	m.handlers[EventChangeChat] = ChangeChatNameHandler
}

func SendMessage(event Event, c *Client) error {
	var chatEvent SendMessageEvent

	if err := json.Unmarshal(event.Payload, &chatEvent); err != nil {
		logger.L.Info(string(event.Payload))
		logger.L.Errorf("bad payload in request: %v", err)
		return err
	}

	logger.L.Info(chatEvent.UserID, chatEvent.ChatID)
	var broadMessage NewMessageEvent

	broadMessage.Sent = time.Now()
	broadMessage.Message = chatEvent.Message
	broadMessage.UserID = chatEvent.UserID
	broadMessage.ChatID = chatEvent.ChatID
	c.currentChatID = chatEvent.ChatID

	data, err := json.Marshal(broadMessage)
	if err != nil {
		return fmt.Errorf("failed to marshal broadcast message: %v", err)
	}

	outgoingEvent := Event{
		Payload: data,
		Type:    EventNewMessage,
	}

	tx := models.DB
	user := &models.User{}

	err = tx.Find(user, chatEvent.UserID)
	if err != nil {
		return err
	}

	chatID, _ := uuid.FromString(chatEvent.ChatID)

	messageModel := models.Message{
		Value:        chatEvent.Message,
		UserID:       user.ID,
		UserNickname: user.Nickname,
		ChatID:       chatID,
	}

	err = tx.Create(&messageModel)
	if err != nil {
		logger.L.Info(err)
		logger.L.Info(messageModel.UserID.String(), messageModel.ChatID.String())
		ack := AckEvent{Name: err.Error()}
		ackMarshalled, _ := json.Marshal(ack)
		event := Event{
			Type:    "ack",
			Payload: ackMarshalled,
		}
		eventMsg, _ := json.Marshal(event)
		err = c.connection.WriteMessage(websocket.TextMessage, eventMsg)
		if err != nil {
			logger.L.Info(err)
		}
	} else {
		ack := AckEvent{Name: "Message written successfully"}
		ackMarshalled, _ := json.Marshal(ack)
		event := Event{
			Type:    "ack",
			Payload: ackMarshalled,
		}
		eventMsg, _ := json.Marshal(event)
		err = c.connection.WriteMessage(websocket.TextMessage, eventMsg)
		if err != nil {
			logger.L.Info(err)
		}

	}

	for client := range c.manager.clients {
		for _, chatID := range client.chatIDs {
			if chatID == c.currentChatID {
				client.egress <- outgoingEvent
			}
		}
	}
	return nil
}

func ChangeChatNameHandler(event Event, c *Client) error {
	var chatRenameEvent ChangeChatNameEvent

	if err := json.Unmarshal(event.Payload, &chatRenameEvent); err != nil {
		logger.L.Info(string(event.Payload))
		logger.L.Errorf("bad payload in request: %v", err)
		return err
	}

	outgoingEvent := Event{
		Payload: event.Payload,
		Type:    EventChangedChatName,
	}

	tx := models.DB
	chat := models.Chat{}

	err := tx.Find(&chat, chatRenameEvent.ChatID)
	if err != nil {
		return err
	}

	chat.Name = chatRenameEvent.Name

	err = tx.Update(&chat)
	if err != nil {
		logger.L.Info(err)
		ack := AckEvent{Name: err.Error()}
		ackMarshalled, _ := json.Marshal(ack)
		event := Event{
			Type:    "ack",
			Payload: ackMarshalled,
		}
		eventMsg, _ := json.Marshal(event)
		err = c.connection.WriteMessage(websocket.TextMessage, eventMsg)
		if err != nil {
			logger.L.Info(err)
		}
	} else {
		ack := AckEvent{Name: "Chat name changed"}
		ackMarshalled, _ := json.Marshal(ack)
		event := Event{
			Type:    "ack",
			Payload: ackMarshalled,
		}
		eventMsg, _ := json.Marshal(event)
		err = c.connection.WriteMessage(websocket.TextMessage, eventMsg)
		if err != nil {
			logger.L.Info(err)
		}

	}

	for client := range c.manager.clients {
		for _, chatID := range client.chatIDs {
			if chatID == chatRenameEvent.ChatID {
				logger.L.Info("sent")
				client.egress <- outgoingEvent
			}
		}
	}

	return nil
}

func (m *Manager) routeEvent(event Event, c *Client) error {
	if handler, ok := m.handlers[event.Type]; ok {
		if err := handler(event, c); err != nil {
			return err
		}
		return nil
	} else {
		return errors.New("there is no such event type")
	}
}

func (m *Manager) ServeWS(c buffalo.Context) error {
	log.Println("new connection")

	userID, ok := c.Session().Get("current_user_id").(uuid.UUID)
	logger.L.Info(userID, ok)
	if !ok {
		return c.Redirect(http.StatusUnauthorized, "/auth")
	}

	conn, err := websocketUpgrader.Upgrade(c.Response(), c.Request(), nil)
	if err != nil {
		log.Println(err)
		return errors.WithStack(err)
	}

	client := NewClient(userID.String(), conn, m)

	m.addClient(client)

	go client.readMessages()
	go client.writeMessages()

	return nil
}

func (m *Manager) addClient(client *Client) {
	m.Lock()
	defer m.Unlock()

	m.clients[client] = true
}

func (m *Manager) removeClient(client *Client) {
	m.Lock()
	defer m.Unlock()

	if _, ok := m.clients[client]; ok {
		client.connection.Close()
		delete(m.clients, client)
	}
}

func checkOrigin(r *http.Request) bool {
	origin := r.Header.Get("Origin")

	logger.L.Info(origin)
	logger.L.Info(app.Addr)

	switch origin {
	case "https://" + app.Addr, "https://gowebchat.byst.re":
		return true
	default:
		return false
	}

}
