package actions

import (
	"fmt"

	"github.com/gobuffalo/buffalo"
	"github.com/gobuffalo/pop/v6"
	"github.com/gofrs/uuid"
	"github.com/golang-jwt/jwt"
	"github.com/pkg/errors"

	"go_webchat/logger"
	"go_webchat/models"
)

// UsersNew renders the users form
func UsersNew(c buffalo.Context) error {
	u := models.User{}
	c.Set("user", u)
	return c.Render(200, r.HTML("users/new.plush.html"))
}

// UsersCreate registers a new user with the application.
func UsersCreate(c buffalo.Context) error {
	u := &models.User{}
	if err := c.Bind(u); err != nil {
		return errors.WithStack(err)
	}

	tx := c.Value("tx").(*pop.Connection)
	verrs, err := u.Create(tx)
	if err != nil {
		c.Set("user", u)
		c.Set("errors", verrs)
		return c.Render(200, r.HTML("users/new.plush.html"))
	}

	if verrs.HasAny() {
		c.Set("user", u)
		c.Set("errors", verrs)
		return c.Render(200, r.HTML("users/new.plush.html"))
	}

	c.Flash().Add("success", "Now you can log in")

	return c.Redirect(302, "/login")
}

func CurrentUser(c buffalo.Context) error {
	uid := c.Session().Get("current_user_id")
	logger.L.Info(uid)
	if uid == nil {
		return c.Render(200, r.JSON(nil))
	}
	u := &models.User{}
	tx := c.Value("tx").(*pop.Connection)
	err := tx.Find(u, uid.(uuid.UUID))
	if err != nil {
		return c.Render(200, r.JSON(nil))
	}
	user := models.User{
		ID:       u.ID,
		Nickname: u.Nickname,
	}
	return c.Render(200, r.JSON(map[string]interface{}{"current_user": user}))
}

func UsersList(c buffalo.Context) error {
	// Get the DB connection from the context
	tx, ok := c.Value("tx").(*pop.Connection)
	if !ok {
		return fmt.Errorf("no transaction found")
	}

	users := &models.Users{}

	if err := tx.All(users); err != nil {
		return err
	}

	return c.Render(200, r.JSON(users))
}

// SetCurrentUser attempts to find a user based on the current_user_id
// in the session. If one is found it is set on the context.
func SetCurrentUser(next buffalo.Handler) buffalo.Handler {
	return func(c buffalo.Context) error {
		if uid := c.Session().Get("current_user_id"); uid != nil {
			u := &models.User{}
			tx := c.Value("tx").(*pop.Connection)
			err := tx.Find(u, uid)
			if err != nil {
				return errors.WithStack(err)
			}
			c.Set("current_user", u)
			c.Set("logged", true)
			c.Set("current_user_nickname", u.Nickname)
		}
		return next(c)
	}
}

// Authorize require a user be logged in before accessing a route
func Authorize(next buffalo.Handler) buffalo.Handler {
	return func(c buffalo.Context) error {
		if uid := c.Session().Get("current_user_id"); uid == nil {
			c.Session().Set("redirectURL", c.Request().URL.String())

			err := c.Session().Save()
			if err != nil {
				return errors.WithStack(err)
			}

			c.Flash().Add("danger", "You must be authorized to see that page")
			return c.Redirect(302, "/auth/new")
		}
		return next(c)
	}
}

// Authorize require a user be logged in before accessing a route
func AuthorizeJWT(next buffalo.Handler) buffalo.Handler {
	return func(c buffalo.Context) error {
		tokenStr, err := c.Cookies().Get("jwt_token")
		if err != nil {
			c.Set("user", models.User{})
			return c.Render(200, r.HTML("auth/new.plush.html"))
		}

		jwtClaims := &JWTClaims{}
		token, err := jwt.ParseWithClaims(
			tokenStr,
			jwtClaims,
			func(token *jwt.Token) (interface{}, error) {
				return JWT, nil
			},
		)

		if err != nil || !token.Valid {
			c.Set("user", models.User{})
			return c.Render(200, r.HTML("auth/new.plush.html"))
		}
		return next(c)
	}
}
