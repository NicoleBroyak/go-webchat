package actions

import (
	"fmt"
	"go_webchat/logger"
	"go_webchat/models"
	"strconv"

	"github.com/gobuffalo/buffalo"
	"github.com/gobuffalo/pop/v6"
)

func ChatMessages(c buffalo.Context) error {
	// Get the DB connection from the context
	tx, ok := c.Value("tx").(*pop.Connection)
	if !ok {
		return fmt.Errorf("no transaction found")
	}

	chatID := c.Params().Get("chat_id")
	offset, _ := strconv.Atoi(c.Params().Get("offset"))
	limit, _ := strconv.Atoi(c.Params().Get("limit"))
	messages := models.Messages{}

	tx.PaginateFromParams(c.Params())

	err := tx.Where("chat_id = ?", chatID).Order("created_at desc").Paginate(offset, limit).All(&messages)
	logger.L.Info(err)
	if err != nil {
		return err
	}

	for i, message := range messages {
		messages[i].TimeFomatted = message.CreatedAt.Local().Format("Mon Jan 2 15:04")
	}

	return c.Render(200, r.JSON(messages))
}

func MessageCreate(c buffalo.Context) error {
	return c.Render(200, r.JSON(""))
}
