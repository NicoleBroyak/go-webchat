package grifts

import (
	"fmt"
	"go_webchat/models"

	"github.com/gobuffalo/grift/grift"
	"github.com/gofrs/uuid"
)

var _ = grift.Namespace("db", func() {

	grift.Desc("seed", "Seeds a database")
	grift.Add("seed", func(c *grift.Context) error {

		nikoID, _ := uuid.FromString("3387d820-4030-4558-a8ca-44baf063f277")
		nikolaID, _ := uuid.FromString("92a7ac5b-8232-48c1-9578-1eed66b9cd0c")
		geeckoID, _ := uuid.FromString("d251f221-f8f1-4e8d-85e8-1cc3f3f5fa0f")
		tx := models.DB
		users := models.Users{
			models.User{
				ID:                   nikoID,
				Nickname:             "Niko",
				Password:             "1111",
				PasswordConfirmation: "1111",
				Email:                "niko@xd.pl",
			},
			models.User{
				ID:                   geeckoID,
				Nickname:             "Geecko",
				Password:             "1111",
				PasswordConfirmation: "1111",
				Email:                "nikola@geecko.la",
			},
			models.User{
				ID:                   nikolaID,
				Nickname:             "Nikola",
				Password:             "1111",
				PasswordConfirmation: "1111",
				Email:                "nikolabroyak@gmail.com",
			},
		}

		for _, user := range users {
			user.Create(tx)
		}

		users2 := models.Users{}

		for i := 0; i < 25; i++ {
			user := models.User{
				Nickname:             fmt.Sprintf("TestUser#%v", i),
				Password:             "1111",
				PasswordConfirmation: "1111",
				Email:                fmt.Sprintf("testuser%v@gmail.com", i),
			}

			users2 = append(users2, user)
		}

		for _, user := range users2 {
			user.Create(tx)
		}

		chatID, _ := uuid.FromString("215d2c41-60b9-4cc3-9055-4165de02a943")

		chats := models.Chats{
			models.Chat{
				ID:   chatID,
				Name: "geecko & niko",
			},
		}

		for _, chat := range chats {
			tx.Create(&chat)
		}

		userChatJunctions := models.UserChatJunctions{
			models.UserChatJunction{
				UserID: nikoID,
				ChatID: chatID,
			},
			models.UserChatJunction{
				UserID: geeckoID,
				ChatID: chatID,
			},
		}

		for _, junction := range userChatJunctions {
			tx.Create(&junction)
		}

		return nil
	})

})
