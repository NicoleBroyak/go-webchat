require("expose-loader?exposes=$,jQuery!jquery")
require("@fortawesome/fontawesome-free/js/all.js")
require("jquery-ujs/src/rails.js")


import { createApp } from 'vue'
import { createRouter, createWebHistory } from "router"
import chatHome from "./pages/chatHome.vue"
import chatView from "./pages/chatView.vue"
import axios from "axios"
import VueAxios from "vueAxios"

const routes = [
  { path: '/', component: chatHome },
  { path: '/chat/:chat_id', component: chatView },
  { path: '/api/chats/with_user/:user_id', component: chatHome },
]

const router = createRouter({
  history: createWebHistory(),
  routes
})

// 5. Create and mount the root instance.
const app = createApp({})
// Make sure to _use_ the router instance to make the
// whole app router-aware.
app.use(router)
app.use(VueAxios, axios)
app.config.globalProperties.window = window

app.mount('#app')
