package logger

import (
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

var L *zap.SugaredLogger

func init() {
	cfg := zap.Config{
		Level:            zap.NewAtomicLevel(),
		Encoding:         "json",
		OutputPaths:      []string{"stdout"},
		ErrorOutputPaths: []string{"stderr"},
		EncoderConfig: zapcore.EncoderConfig{
			CallerKey:    "line",
			MessageKey:   "msg",
			LevelKey:     "lvl",
			EncodeLevel:  zapcore.CapitalLevelEncoder,
			EncodeCaller: zapcore.ShortCallerEncoder,
		},
	}

	l, err := cfg.Build(zap.AddCaller())
	L = l.Sugar()
	if err != nil {
		panic(err)
	}
	defer L.Sync()

	L.Info("logger construction succeeded")
}
